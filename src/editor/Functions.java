package editor;


public class Functions {
	private Texed texed;
	private StackAdapter<String> closingTags, undo, redo;
	
	//variables
	private boolean tagFlag;
	private int tagClose;
	private String closingTag;
	private boolean tagFound;
	
	/**
	 * Constructor for class Functions
	 * @param texed for link with GUI
	 */
	public Functions(Texed texed)
	{
		this.texed = texed;
		closingTags = new StackAdapter<String>();
		undo = new StackAdapter<String>();
		redo = new StackAdapter<String>();
	}
	
	/**
	 * push document event to undo stack
	 * @param text
	 */
	public void addUndo(String text)
	{
		undo.push(text);
		while(!redo.isEmpty())
		{
			redo.pop();
		}
	}
	
	/**
	 * First push top element to redo stack
	 * then delete top element of undo stack
	 * return next top element
	 * 
	 * @return
	 */
	public String undo()
	{
		if(!undo.isEmpty())
		{
			redo.push(undo.top());
			undo.pop();
			if (undo.isEmpty()) return "";
			return undo.top();
		}
		else return null;
		
	}
	
	/**
	 * Return top element of redo stack
	 * push top element to undo stack
	 * delete top element of redo stack
	 * if stack is empty return top of undo stack (same as what is already on screen)
	 * @return
	 */
	public String redo()
	{
		if(!redo.isEmpty())
		{
			undo.push(redo.top());
			String temp = redo.top();
			redo.pop();
			return temp;
		}
		else return undo.top();
	}
	
	
	
	/**
	 * Getter for temporary closing tag
	 * @return String
	 */
	public String getClosingTag()
	{
		String temp = closingTag;
		closingTag = null;
		return temp;
	}
	
	/**
	 * method to check if redo stack is empty/contains just one element
	 * @return
	 */
	public boolean isRedoEmpty()
	{
		boolean empty = false;
		if (redo.size()<2) empty = true;
		return empty;
	}
	
	/**
	 * Check if open tag for completion is found
	 * @return
	 */
	public boolean isTagFound()
	{
		return tagFound;
	}
	
	/**
	 * set boolean tagFound to false before going into loop
	 * in method addClosingTag() in Texed
	 */
	public void setTagFound()
	{
		tagFound = false;
	}
	
	/**
	 * Method for searching last opened and not yet closed tag
	 */
	public void searchOpenTag(String str, int offset)
	{
		/**
		 * Check for closing bracket
		 */
		if (str.equals(">")) 
		{
			if(tagFlag) 
				{
					texed.setComment("ERROR: MULTIPLE CLOSING BRACKETS");
				}
			tagClose = offset-1;
			tagFlag = true;
		}
		
		/**
		 * Check for open bracket
		 */
		if (str.equals("<"))
		{
			if(!tagFlag) texed.setComment("ERROR: MULTIPLE OPEN BRACKETS");
			/**
			 * check if the tag is a closing tag
			 * if so add tag to stack: closingTags
			 */
			if (texed.getText(offset+1, 1).equals("/"))
			{
				closingTags.push(texed.getText(offset+2, (tagClose-offset-1)));
			}
			
			/**
			 * check if an open tag is already closed
			 * look up in stack ClosingTags if tag equals top
			 */
			else if ( !(closingTags.isEmpty()) && texed.getText(offset+1, (tagClose-offset)).equals(closingTags.top()))
			{
				closingTags.pop();
			}
			
			/**
			 * else create closing tag
			 */
			else {
				closingTag = texed.getText(offset+1, (tagClose-offset));
				tagFound = true;
			}
			tagFlag = false;
		}
	}
}

