package editor;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

/**
 * Simple GUI for a text editor.
 *
 */
public class Texed extends JFrame implements DocumentListener, ActionListener, KeyListener {
	private JPanel panel, buttons, commentpanel;
	private JTextArea textArea;
	private JLabel commenthead,comment;
	private JButton undo, redo, check;
	private Functions functions;
	
	//variables
	private boolean buttonPressed;

	private static final long serialVersionUID = 5514566716849599754L;
	/**
	 * Constructs a new GUI: A TextArea on a ScrollPane
	 */
	public Texed() {
		//Setup Buttons
		undo = new JButton("UNDO");
		redo = new JButton("REDO");
		check = new JButton("CHECK");
		setupButtons();
		buttons = new JPanel();
		buttons.setLayout(new GridLayout(1,3));
		buttons.add(undo);
		buttons.add(redo);
		buttons.add(check);
		
		//Setup TextArea
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		//Registration of the callback
		textArea.getDocument().addDocumentListener(this);
		JScrollPane scrollPane = new JScrollPane(textArea);
		
		//Setup Comment Section
		commentpanel = new JPanel();
		commenthead = new JLabel("- - - - - - - - -   COMMENT SECTION   - - - - - - - - -");
		comment = new JLabel("   ");
		comment.setHorizontalAlignment(getWidth()/2);
		commentpanel.setLayout(new BorderLayout());
		commentpanel.add(commenthead, BorderLayout.NORTH);
		commentpanel.add(comment, BorderLayout.CENTER);
		
		//Setup JPanel
		panel = new JPanel();
		panel.setLayout(new BorderLayout());
		textArea.addKeyListener(this);
		add(panel);
		setTitle("Texed: simple text editor");
		setBounds(10, 10, 800, 600);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//add JComponents
		panel.add(buttons, BorderLayout.SOUTH);
		panel.add(scrollPane,BorderLayout.CENTER);
		panel.add(commentpanel,BorderLayout.EAST);
		
		//link class functions with GUI
		functions = new Functions(this);
	}
	
	/**
	 * Setup buttons with actionListener
	 */
	private void setupButtons()
	{
		//undo ActionListener
		undo.addActionListener(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					buttonPressed = true;
					textArea.setText(functions.undo());
					buttonPressed = false;
				}
			});
		
		//redo ActionListener
		redo.addActionListener(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					buttonPressed = true;
					textArea.setText(functions.redo());
				}
			});
				
		//check ActionListener
		check.addActionListener(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					/*
					 * First set comment to no errors, if error detected: comment will change
					 * search for multiple open/closing tags
					 * search for not closed tag
					 */
					comment.setText("No errors detected");
					String karakter = "" ;
					for (int i = textArea.getText().length()-1; i>=0 ;i--) {
						if(functions.getClosingTag() != null && functions.isTagFound())
						{
							comment.setText("ERROR: compiling did not succeed");
							break;
						}
						else {
							try {
								karakter = textArea.getText(i,1);
							} catch (BadLocationException c) {
								c.printStackTrace();
							}
						}
						functions.searchOpenTag(karakter,i);
					}
				}
			});
	}

	
	/**
	 * Callback when changing an element
	 */
	public void changedUpdate(DocumentEvent ev) {
		if (!buttonPressed) functions.addUndo(textArea.getText());
	}

	/**
	 * Callback when deleting an element
	 */
	public void removeUpdate(DocumentEvent ev) {
		
		if (!buttonPressed) functions.addUndo(textArea.getText());
	}

	/**
	 * Callback when inserting an element
	 */
	public void insertUpdate(DocumentEvent ev) {
		//Check if the change is only a single character, otherwise return so it does not go in an infinite loop
		if(ev.getLength() != 1) return;		
		
		
		if (!buttonPressed) functions.addUndo(textArea.getText());
	}
	
	/**
	 * Method AutoCompletion closing tags
	 * Search backwards for last opened and not yet closed tag
	 */
	public void addClosingTag()
	{
		int cursor = textArea.getCaretPosition();
		String karakter = "";
		functions.setTagFound();
		for (int i = (cursor-1); i>=0; i--)
		{
			if(functions.isTagFound())
			{
				break;
			}
			else {
				try {
					karakter = textArea.getText(i,1);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
			}
			functions.searchOpenTag(karakter,i);
		}
		String s = functions.getClosingTag();
		if (s != null) {SwingUtilities.invokeLater(new Task(("</" + s + ">"), cursor));
		}
	}
	
	/**
	 * method for adding comments to JPanel
	 */
	public void setComment(String s)
	{
		comment.setText(s);
	}
	
	/**
	 * returns text from textArea from cursor position and given length
	 * @param offset
	 * @param length
	 * @return String
	 */
	public String getText(int offset, int length)
	{
		try {
			return textArea.getText(offset,length);
		} catch (BadLocationException e) {
			e.printStackTrace();
			return null;
		}
	}
	

	/**
	 * Runnable: change UI elements as a result of a callback
	 * Start a new Task by invoking it through SwingUtilities.invokeLater
	 */
	private class Task implements Runnable {
		private String text;
		private int position;
		
		/**
		 * Pass parameters in the Runnable constructor to pass data from the callback 
		 * @param text
		 * @param position
		 * text will be inserted on given position (cursor)
		 */
		Task(String text,int position) {
			this.text = text;
			this.position = position;
		}

		/**
		 * The entry point of the runnable
		 */
		public void run() {
			//textArea.append(text);
			textArea.insert(text, position);
		}
	}
	
	/**
	 * Entry point of the application: starts a GUI
	 */
	public static void main(String[] args) {
		new Texed();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int k = e.getKeyCode();
		if(k == 9 ) 
		{
			addClosingTag();
		}
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
